-- apis/BasketNet2

local tThreads = {} 

function broadcast(tRequest, sProtocol)
	
	if not rednet.isOpen("left") then
		rednet.open("left")
	end

	if sProtocol == nil then
		sProtocol = "BasketNet2"
	end
	
	-- Build Request Object
	local tRequest = {}
	tRequest.bIsRequest = true
	tRequest.sMessageID = getRandomString(8)
	if tRequest[sThreadID] == nil then
		tRequest.sThreadID = getRandomString(8)
	end
	
	rednet.broadcast(tRequest, sProtocol)
			
end

function send(iRecipientID, tRequest, sProtocol, iTimeout, iMaxAttempts)
	
	if not rednet.isOpen("left") then
		rednet.open("left")
	end
	
	if sProtocol == nil then
		sProtocol = "BasketNet2"
	end
	
	if iTimeout == nil then
		iTimeout = 1
	end

	if iMaxAttempts == nil then
		iMaxAttempts = 3
	end
	
	if type(tRequest) ~= "table" then
		return nil
	end
	
	-- Build Request Object
	tRequest.bIsRequest = true
	tRequest.sMessageID = getRandomString(8)
	if tRequest[sThreadID] == nil then
		tRequest.sThreadID = getRandomString(8)
	end
	
	local iAttempts = 0
	
	iRecipientID = iRecipientID + 0
	if iRecipientID ~= nil and iRecipientID ~= 0 then
		while iAttempts < iMaxAttempts do
			rednet.send(iRecipientID, tRequest, sProtocol)
			local iSenderID, tResponse = rednet.receive("BasketNet2_" .. tRequest.sThreadID, iTimeout)
			if tResponse == "OK" and iSenderID == iRecipientID then
			
				local tResult = {}
		
				tResult.iRecipientID = iRecipientID
				
				tResult.tPayload = tResponsePayload

				tResult.sThreadID = tResponse.tPayload.sThreadID
				
				tResult.receive = function(sProtocol, iTimeout)
					if sProtocol == nil then
						sProtocol = "BasketNet2_" .. tResult.sThreadID
					end
					return receive(sProtocol, iTimeout)
				end				
				
				return tResult
				
			end
			iAttempts = iAttempts + 1
		end
	end
	
	return nil
end

function receive(sProtocol, iTimeout)
	
	if not rednet.isOpen("left") then
		rednet.open("left")
	end

	if sProtocol == nil then
		sProtocol = "BasketNet2"
	end

	if iTimeout == nil then
		iTimeout = 3
	end
	
	local iSenderID, tResponsePayload = rednet.receive(sProtocol, iTimeout)
	
	if iSenderID ~= nil then
	
		-- Confirm that you got the message
		rednet.send(iSenderID, "OK", "BasketNet2_" .. tResponsePayload.sThreadID)
		
		local tResponse = {}
	
		tResponse.bIsResponse = true
		
		tResponse.iSenderID = iSenderID
		
		tResponse.tPayload = tResponsePayload
		
		tResponse.reply = function(tRequest, sProtocol, iTimeout, iMaxAttempts)
			tRequest.sThreadID = tResponse.tPayload.sThreadID
			if sProtocol == nil then
				sProtocol = "BasketNet2_" .. tRequest.sThreadID
			end
			return send(tResponse.iSenderID, tRequest, sProtocol, iTimeout, iMaxAttempts)
		end
		
		return tResponse	
	
	end
	
	return nil
	
end

function getComputerID(name)
	local iComputerID = false
	while true do
		iComputerID = rednet.lookup("BasketNet2", name)
		if not iComputerID then
			print("Unable to find " .. name)
			sleep(5)
		else
			break
		end
	end
	return iComputerID
end

function registerAs(name)
	rednet.host("BasketNet2", name)
end

function getRandomString(l)
        if l < 1 then
		return nil
	end
        local sKey = ""
        for i = 1, l do
                sKey = sKey .. string.char(math.random(string.byte("a"), string.byte("z")))
        end
        return sKey
end