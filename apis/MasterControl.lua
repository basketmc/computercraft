-- apis/MasterControl

os.loadAPI("apis/BasketNet2")
os.loadAPI("apis/ChestManagement")
os.loadAPI("apis/Job")

local tComputerNames = {}

local tStorageCells = {}

local tJobQueue = {}

function init()
	if not rednet.isOpen("left") then
		rednet.open("left")
	end

	BasketNet2.registerAs("MasterControl")

	BasketNet2.broadcast("reboot")
end

function processRequest()

	local tRequest = BasketNet2.receive(60)
	
	if tRequest == nil then
		return false
	end
	
	print("Request from " .. tRequest.iSenderID .. ":")
	print("\tAction: " .. tRequest.action)
	
	
	if tRequest.action == "deposit" then
	
	
		-- create job
		   -- determine what storage cell isn't busy
		      -- build subjobs to to move items along path
		
	
	
	
	
	

	elseif tRequest.action == "RequestItem" then
		processItemRequest(tRequest)
		
		
		

	elseif tResponse.action == "fuelrequest" then
		processFuelRequest(tResponse)
		
		
		
		
		
		
	elseif tResponse.action == "storagecell" then
		tStorageCells[tResponse.cell + 0] = tResponse.iSenderID
		
	elseif tResponse.action == "storagecelllist" then
		--BasketNet2.reply(getListOfStorageCells())
		
	elseif tResponse.action == "getinventory" then
		BasketNet2.reply(getInventory(tResponse.storagecell))
		
	elseif tResponse.action == "reboot" then
		os.reboot()
		
	--elseif sMessage == "RefuelingComplete" then
	--	processFuelRequest(iSenderID)
	
	elseif tResponse.action == "retrieve" then
		createRetrieveJob(tResponse)
		
	end
	
end

function createRetrieveJob(tResponse)
	for iStorageCellID, iComputerID in pairs(tStorageCells) do
		BasketNet.send(iComputerID, tLastParsedMessage[1] .. " " .. tLastParsedMessage[2] .. " " .. tLastParsedMessage[3], "BasketNet")
		BasketNet.send(iComputerID, sMessage, "BasketNetRetrieve")
		local iSenderID, sMessage, sProtocol = BasketNet.receive(3, "BasketNetRetrieve")
		if iSenderID ~= nil and sMessage == "Retrieving" then
			BasketNet.sendToConversation("Retrieving")
		end
		break
	end
end

function getListOfStorageCells()
	
	local tStorageCellNames = {}

	for iStorageCellID, iComputerID in pairs(tStorageCells) do
		tStorageCellNames[iStorageCellID] = {}
		tStorageCellNames[iStorageCellID]["iComputerID"] = iComputerID
		tStorageCellNames[iStorageCellID]["sName"] = tComputerNames[iComputerID]
	end

	return tStorageCellNames

end

function getInventory(sOwnerName)
	local tItems = {}
	for iStorageCellID, iComputerID in pairs(tStorageCells) do
		-- TODO check if the Storage Cell is owned by sOwnerName
		local iAttempts = 0
		while iAttempts < 5 do
			print("Requesting Inventory from " .. tStorageCells[iStorageCellID])
			BasketNet.send(iComputerID, "GetInventory", "BasketNet")
			local iSenderID, sMessage, sProtocol = BasketNet.receive(5, "BasketNetGetInventory")
			if iSenderID == iComputerID then  
				print("\t\tInventory Received")                                   
				iAttempts = 0
				local tItems2 = {}
				for iChestID, tChestItems in ipairs(sMessage) do
					for k, iItem in ipairs(tChestItems) do
						table.insert(tItems2, iItem)
					end
				end
				tItems = combineItemLists(tItems, tItems2)
				break
			end
			iAttempts = iAttempts + 1
		end
	end
	return tItems
end

function combineItemLists(tItems1, tItems2)
	tItems1 = condenseItemList(tItems1)
	tItems2 = condenseItemList(tItems2)
	for k1, tItem1 in pairs(tItems1) do
		for k2, tItem2 in pairs(tItems2) do
			if ChestManagement.itemsAreTheSame(tItem1, tItem2) then
				tItems[k1].count = tItems[k1].count + tItems[k2].count
				table.remove(tItems2, k2)
			end
		end
	end
	for k, tItem2 in pairs(tItems2) do
		table.insert(tItems1, tItem2)
	end
	return tItems1
end

function condenseItemList(tItems)
	for k1, tItem1 in pairs(tItems) do
		for k2, tItem2 in pairs(tItems) do
			if k1 ~= k2 and ChestManagement.itemsAreTheSame(tItem1, tItem2) then
				tItems[k1].count = tItems[k1].count + tItems[k2].count
				table.remove(tItems, k2)
			end
		end
	end
	return tItems
end

function processComputerName(iComputerID, sComputerName)
	tComputerNames[iComputerID] = sComputerName
	BasketNet.send(iComputerID, "NameReceived", "BasketNetAck")
end

function processFuelRequest(iSenderID)
	
	BasketNet.send(iStorageID, "RequestReceived", "BasketNet")
	print(iSenderID .. " wants fuel.  Place it in their chest and hit enter")
	read("*")

	BasketNet.send(iStorageID, "Refuel", "BasketNet")

end

function processFuelingCompleteRequest(iSenderID)
	
	BasketNet.send(iStorageID, "RequestReceived", "BasketNet")
	print(iSenderID .. " has refueld.  Please remove the empty buckets and hit enter")
	read("*")

	BasketNet.send(iStorageID, "Clear", "BasketNet")

end

function processItemMoveRequest(sSourceType, iSourceID, sDestinationType, sDestinationID, sItemName, iAmount)

end

function isTargetReady(iTarget)

	BasketNet.send(iTarget, "Ready?", "BasketNet")
	while true do
		local iSenderId, sMessage, sProtocol = BasketNet.receive(3, "BasketNetReadyCheck")

		-- If message not from iTarget
		if iSenderId == iTarget then

			-- If timeout
			if iSenderId == nil then
				return false
			end

			-- If replied back with not ready
			if sMessage ~= "Ready" then
				return false
			end

			return true

		end
	end

end

function sendStorageCellStoreBufferRequest(iStorageCellID)
	if not isTargetReady(iStorageCellID) then
		return false
	end	
	
	BasketNet.send(iStorageCellID, "Store", "BasketNet")

	return true
end

function sendStorageCellFillBufferRequest(iStorageCellID, sItem, iAmount)
	if not isTargetReady(iStorageCellID) then
		return false
	end	
	
	BasketNet.send(iStorageCellID, "Retrieve " .. sItem .. " " .. iAmount, "BasketNet")

	return true
end