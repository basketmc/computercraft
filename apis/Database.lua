-- apis/Database

os.loadAPI("apis/BasketNet2")

local iDatabaseServerID

function saveTable(name, table)
	local file = fs.open("dbs/" .. name, "w")
	file.write(textutils.serialize(table))
	file.close()
end

function loadTable(name)
	local file = fs.open("dbs/" .. name, "r")
	if file == nil then
		return nil
	end
	local data = file.readAll()
	file.close()
	return textutils.unserialize(data)
end

function getDatabaseServerID()
	if iDatabaseServerID == nil or not iDatabaseServerID then
		while true do
			iDatabaseServerID = rednet.lookup("BasketNet", "DatabaseServer")
			if not iDatabaseServerID then
				print("Unable to find Database Server.")
				sleep(2)
			else
				break
			end
		end
	end
	return iDatabaseServerID
end

function getFromServer(name)
	local tRequest = {}
	tRequest.action = "get"
	tRequest.name = name
	local tResult = send(getDatabaseServerID(), tRequest)	
	if tResult == nil then
		return false
	end
	
	local tResponse = tResult.receive()	
	if tResponse == nil then
		return false
	end	
	return tResponse.value	
end

function putOnServer(name, value)
	local tRequest = {}
	tRequest.action = "put"
	tRequest.name = name
	tRequest.value = value
	local tResult = send(getDatabaseServerID(), tRequest)	
	if tResult == nil then
		return false
	else
		return true
	end
end
