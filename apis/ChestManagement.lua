-- apis/ChestManagement

-- Print what is inside the chest in front of the turtle
function whatIsInChest()
	
	if turtle.detectDown() then
		turtle.digDown()
	end
	
	turtle.select(16)
	turtle.placeDown()
	turtle.select(1)
	
	-- Move everything from the chest in front to the swap chest below
	while turtle.suck() do
		if not turtle.dropDown() then
			-- The swap chest is smaller than the source chest :(
			print("Bottom chest is full??  Reverting")
			while turtle.suckDown() do
				turtle.drop()
			end
			return false
		end
	end
	
	local contents = {}
	
	-- Move everything from the swap chest back into the chest in front
	while turtle.suckDown() do
		local info = turtle.getItemDetail(1)

		-- Look through existing content to see if it matches
		for k, tItem1 in pairs(contents) do
			if itemsAreTheSame(tItem1, info) then				
				contents[k].count = contents[k].count + info.count
				info = nil
				break
			end
		end

		if info ~= nil then
			table.insert(contents, info)
		end
	
		turtle.drop()
	end
	
	-- Put the swap chest back into the inventory
	turtle.select(16)
	turtle.digDown()
	turtle.select(1)
	
	for k, v in pairs(contents) do
		for k2, v2 in pairs(v) do
			print(k2 .. ": " .. v2)
		end
		print(" ")
	end
	
	return contents

end

function itemsAreTheSame(tItem1, tItem2)

	if tItem1 == nil then
		return false
	end

	if tItem2 == nil then
		return false
	end

	for k, v in pairs(tItem1) do
		if k ~= "count" and tItem1[k] ~= tItem2[k] then
			return false
		end
	end

	-- Check it based on the keys for tItem2 just in case it has some extra important key
	for k, v in pairs(tItem2) do
		if k ~= "count" and tItem1[k] ~= tItem2[k] then
			return false
		end
	end

	return true
end

-- Prompt the user to enter colors for the ender chest
function createEnderChestConfig()

	print(" ")
	
	print("Configuring...")
	
	local tChest = {}
	tChest.sType = "EnderChest"
	tChest.tCode = {}
	
	local i = 0
	
	local tDyes = {}
	tDyes["1"] = "#191919" --Black
	tDyes["2"] = "#993333" --Red
	tDyes["3"] = "#667F33" --Green
	tDyes["4"] = "#664C33" --Brown
	tDyes["5"] = "#334CB2" --Blue
	tDyes['6"] = "#7F3FB2" --Purple
	tDyes["7"] = "#4C7F99" --Cyan
	tDyes["8"] = "#999999" --Light Gray
	tDyes["9"] = "#4C4C4C" --Gray
	tDyes["10"] = "#F27FA5" --Pink
	tDyes["11"] = "#7FCC19" --Lime
	tDyes["12"] = "#E5E533" --Yellow
	tDyes["13"] = "#6699D8" --Light Blue
	tDyes["14"] = "#B24CD8" --Magenta
	tDyes["15"] = "#D87F33" --Orange
	tDyes["16"] = "#FFFFFF" --White		
	
	for i = 0, 2, 1 do
	
		while true do
		
			print("Please select the color of " .. (i + 1) .. ":")
	
			print(" 1. Black        9. Gray")
			print(" 2. Red         10. Pink")
			print(" 3. Green       11. Lime")
			print(" 4. Brown       12. Yellow")
			print(" 5. Blue        13. Light Blue")
			print(" 6. Purple      14. Magenta")
			print(" 7. Cyan        15. Orange")
			print(" 8. Light Gray  16. White")
			print(" 0. Exit")
	
			choice = read()
	
			if choice == "0" then
				return false
				
			elseif tDyes[choice] ~= nil then
				tChest.tCode[i] = tDyes[choice]
				break
				
			else
				print("Unknown selection...")
				sleep(3)
			end
			
		end
		
	end
	
	return tChest

end
