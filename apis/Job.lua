-- apis/Job

os.loadAPI("apis/BasketNet2")

local iJobQueuManger = null
	
while true do
	iJobQueuManger = rednet.lookup("BasketNet", "JobQueue")
	if iJobQueuManger ~= nil then
		break		
	end
	term.clear()
	print("Unable to connect to Job Queue Server")
	sleep(5)
end

function createJobRequest(iJobRequester, iJobRecipient, tRequest)

	local tJobRequest = {}
	
	tJobRequest.sJobKey = createJobKey(10)
	
	tJobRequest.iJobRequester = iJobRequester
	
	tJobRequest.iJobRecipient = iJobRecipient
	tJobRequest.tRequest = tRequest
	
	tJobRequest.sStatus = "pending"
	
	tJobRequest.sTaskDescription = tResponse.sTaskDescription
	
	tJobRequest.startJob = function()
		return false
	end
	
	tJobRequest.jobReady = function()
		if not isTargetReady(tJobRequest.iJobRecipient) then
			return false		
		end
		for sChildJobKey, tChildJob in pairs(tJobRequest.tSubJobs) do
			if not tChildJob.jobReady() then
				return false
			end
		end		
		return true
	end
	
	tJobRequest.sendJob = function()
		local tResult = BasketNet2.send(tJobRequest.iJobRecipient, tJobRequest.tRequest)
		if tResult ~= nil then
			tJobRequest.sStatus = "sent"
			return true
		end
		return false
	end
	
	tJobRequest.tSubJobs = {}
	
	return tJobRequest

end