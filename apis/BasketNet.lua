-- apis/BasketNet

local iMasterControlID

local iConversationPartnerID
local sConversationKey

function broadcast(sMessage, sProtocol)
	if sProtocol == nil then
		sProtocol = "BasketNet"
	end
	if not rednet.isOpen("left") then
		rednet.open("left")
	end
	rednet.broadcast(sMessage, sProtocol)
end

function send(id, sMessage, sProtocol)
	if sProtocol == nil then
		sProtocol = "BasketNet"
	end
	if not rednet.isOpen("left") then
		rednet.open("left")
	end
	id = id + 0
	if id ~= nil and id ~= 0 then
		return rednet.send(id, sMessage, sProtocol)
	end
	return false
end

function sendToMasterControl(sMessage, sProtocol)	
	if sProtocol == nil then
		sProtocol = "BasketNet"
	end
	if not rednet.isOpen("left") then
		rednet.open("left")
	end
	return rednet.send(getMasterControlID(), sMessage, sProtocol)
end

-- Return local iSenderId, sMessage, sProtocol
-- on timeout returns nil
function receive(iTimeout, sProtocol)
	if sProtocol == nil then
		sProtocol = "BasketNet"
	end
	return rednet.receive(sProtocol, iTimeout)
end

-- Return local iSenderId, sMessage, sProtocol
-- on timeout returns nil
function receiveFromMasterControl(iTimeout, sProtocol)
	local iSenderID, sMessage, sReceivedProtocol
	while true do
		if sProtocol == nil then
			sProtocol = "BasketNet"
		end
		iSenderID, sMessage, sReceivedProtocol = rednet.receive(sProtocol, iTimeout)
		if iSenderID == nil then
			return nil
		end
		if iSenderID == getMasterControlID() then
			break
		end
	end

	return iSenderID, sMessage, sReceivedProtocol
end

function getMasterControlID()
	if iMasterControlID == nil or not iMasterControlID then
		while true do
			iMasterControlID = rednet.lookup("BasketNet", "MasterControl")
			if not iMasterControlID then
				print("Unable to find Master Control.")
				sleep(5)
			else
				break
			end
		end
	end
	return iMasterControlID
end

function registerAsMasterControl()
	rednet.host("BasketNet", "MasterControl")
end

----------------------------
-- Conversation Functions --
----------------------------

function startConversation(iComputerID)
	sConversationKey = getConversationKey(10)
	iConversationPartnerID = iComputerID
	local iAttempts = 0
	while iAttempts < 5 do
		send(iComputerID, "StartConversation " .. sConversationKey, "BasketNet")
		local iSenderID, sMessage, sProtocol = receiveFromConversation(1)
		if iSenderID ~= nil and sMessage == "JoinedConversation" then
			iAttempts = 0
			break
		end
		iAttempts = iAttempts + 1
	end
	if iAttempts == 0 then
		return true
	else
		return false
	end
end

function startConversationWithMasterControl()
	return startConversation(getMasterControlID())
end

function joinConversation(iNewConversationPartnerID, sNewConversationKey)
	iConversationPartnerID = iNewConversationPartnerID
	sConversationKey = sNewConversationKey
	sendToConversation("JoinedConversation")
end

function endConversation()
	sendToConversation("EndConversation")
	iConversationPartnerID = 0
	sConversationKey = ""
end

-- Return local iSenderID, sMessage, sProtocol
-- on timeout returns nil
function receiveFromConversation(iTimeout)
	if iTimeout == nil then
		iTimeout = 2
	end
	return rednet.receive("BasketNet" .. sConversationKey, iTimeout)
end

function sendToConversation(sMessage)
	if iConversationPartnerID ~= 0 then
		send(iConversationPartnerID, sMessage, "BasketNet" .. sConversationKey)
	end
end

function getConversationKey(l)
        if l < 1 then
		return nil
	end
        local sKey = ""
        for i = 1, l do
                sKey = sKey .. string.char(math.random(string.byte("a"), string.byte("z")))
        end
        return sKey
end

