-- apis/StorageCell

os.loadAPI("apis/BasketNet2")
os.loadAPI("apis/Database")
os.loadAPI("apis/Turtle2")
os.loadAPI("apis/Coordinates")

local tConfig
	
local iMasterControlID
local iJobQueuManger

function init()

	if not rednet.isOpen("left") then
		rednet.open("left")
	end
	
	tConfig = Database.loadTable("config")
	
	term.clear()
	
	iMasterControlID = BasketNet2.getComputerID("MasterControl")
	
	iJobQueuManger = BasketNet2.getComputerID("JobQueue")
	
	Turtle2.init()
	Turtle2.determineFacingDirection()
		
	while Turtle2.getCoords("home1") == nil or Turtle2.getCoords("home2") == nil or Turtle2.getCoords("home3") == nil do
		promptForHomeLocations()
	end
	
	returnHome()

	if turtle.getFuelLevel() < 1000 then
		print("I'm low on fuel")
		return
	end
	
end

function promptForHomeLocations()

	print("Home locations not set...")
	sleep(1)
	
	local choice = "menu"
	
	while true do
	
		term.clear()
		
		print("Current Values:")
		
		if Turtle2.getCoords("home1") == nil and Turtle2.getCoords("home2") == nil and Turtle2.getCoords("home3") == nil then
			
			print("-- None --")
		
		else
		
			if Turtle2.getCoords("home1") ~= nil then
				print("Home 1: " .. Turtle2.getCoords("home1").print())
			end
			
			if Turtle2.getCoords("home2") ~= nil then
				print("Home 2: " .. Turtle2.getCoords("home2").print())
			end
			
			if Turtle2.getCoords("home3") ~= nil then
				print("Home 3: " .. Turtle2.getCoords("home3").print())
			end
		
		end
		
		print(" ");		

		print("Menu:")

		print(" 1. Set Current Location as Home 1")

		print(" 2. Set Current Location as Home 2")

		print(" 3. Set Current Location as Home 3")

		print(" 0. Exit")

		choice = read()

		if choice == "0" then
			return
		else		
			local coords = Turtle2.setCoords("home" .. choice)		
			tConfig["home" .. choice] = coords;			
			Database.saveTable("config", tConfig)			
		end
		
	end

end

function returnHome()
	
	local tHomes = {}
	
	tHomes[1] = Turtle2.getCoords("home1")
	tHomes[2] = Turtle2.getCoords("home2")
	tHomes[3] = Turtle2.getCoords("home3")
	
	local tFarCorner1 = tHomes[1].clone()
	local tFarCorner2 = tHomes[3].clone()
	
	if tHomes[1].x == tHomes[3].x then
		if math.fmod(tConfig.StorageCellBus, 4) == 1 then		
			tFarCorner1.x = tFarCorner1.x + 16
			tFarCorner2.x = tFarCorner2.x + 16
		elseif math.fmod(tConfig.StorageCellBus, 4) == 3 then
			tFarCorner1.x = tFarCorner1.x - 16
			tFarCorner2.x = tFarCorner2.x - 16
		end
	end
	
	if tHomes[1].z == tHomes[3].z then
		if math.fmod(tConfig.StorageCellBus, 4) == 2 then	
			tFarCorner1.z = tFarCorner1.z + 16
			tFarCorner2.z = tFarCorner2.z + 16
		elseif math.fmod(tConfig.StorageCellBus, 4) == 0 then
			tFarCorner1.z = tFarCorner1.z - 16
			tFarCorner2.z = tFarCorner2.z - 16
		end
	end
	
	if Turtle2.getCurrentLocation().betweenPoints(tHomes[1], tFarCorner1) then
		Turtle2.goTo(tFarCorner1)
	end
	
	if Turtle2.getCurrentLocation().isEqual(tFarCorner1) or Turtle2.getCurrentLocation().betweenPoints(tFarCorner1, tFarCorner2) then
		Turtle2.goTo(tFarCorner2)
	end
	
	if Turtle2.getCurrentLocation().isEqual(tFarCorner2) or Turtle2.getCurrentLocation().betweenPoints(tFarCorner2, tHomes[3]) then
		Turtle2.goTo(tHomes[3])
	end
	
	-- If it's in position 3
	if tHomes[3].isEqual(Turtle2.getCurrentLocation()) then
	
		local iRotateTo = tHomes[3].r
		
		if math.fmod(tConfig.StorageCellBus, 2) == 1 then
			iRotateTo = iRotateTo + 1 -- rotate Right of Home 3
		else
			iRotateTo = iRotateTo - 1 -- rotate Left of Home 3
		end
		
		iRotateTo = math.fmod(iRotateTo + 4, 4)

		Turtle2.rotateTo(iRotateTo)
				
		if not Turtle2.isFacingTurtle() then
			Turtle2.goTo(tHomes[2])
		end
		
	end
	
	-- If it's in position 2
	if tHomes[2].isEqual(Turtle2.getCurrentLocation()) then
	
		local iRotateTo = tHomes[2].r
		
		if math.fmod(tConfig.StorageCellBus, 2) == 1 then
			iRotateTo = iRotateTo + 1 -- rotate Right of Home 2
		else
			iRotateTo = iRotateTo - 1 -- rotate Left of Home 2
		end
		
		iRotateTo = math.fmod(iRotateTo + 4, 4)

		Turtle2.rotateTo(iRotateTo)
				
		if not Turtle2.isFacingTurtle() then
			Turtle2.goTo(tHomes[1])
		end
		
	end
	
	if tHomes[1].isEqual(Turtle2.getCurrentLocation()) then
		Turtle2.rotateTo(tHomes[1].r)	
	elseif tHomes[2].isEqual(Turtle2.getCurrentLocation()) then
		Turtle2.rotateTo(tHomes[2].r)
	elseif tHomes[3].isEqual(Turtle2.getCurrentLocation()) then
		Turtle2.rotateTo(tHomes[3].r)
	end

	return true
end

function rotateToEnderChest()
	while true do
		if Turtle2.isFacingEnderChest() then
			return true
		end
				
		-- Rotate to find the ender chest, rotate more efficiently based on bus orientation
		if math.fmod(tConfig.StorageCellBus, 2) == 1 then
			Turtle2.turnLeft()
		else
			Turtle2.turnRight()
		end	
	end
end
 
function processRequest()

	local tResponse = BasketNet2.receive(nil, 60)
	
	if tResponse == nil then
		return false
	end
	
	print("Request from " .. tResponse.iSenderID .. ":")
	print("\tAction: " .. tResponse.action)

	if tResponse.action == "busy" then
		tResponse.reply({answer = "no"})
		
	elseif tResponse.action == "store" then
		storeBuffer()
		
	elseif tResponse.action == "retrieve" then
		tResponse.reply({action = "retrieving"})
		print("\t\tRetrieving " .. tResponse.amount .. "x " .. tResponse.item.name)		
		retrieveItems(tResponse.owner, tResponse.item, tResponse.amount)
		
	elseif tResponse.action == "reboot" then
		os.reboot()
		
	end
end

function registerWithJobManager()

	local tPayload = {}
	
	tPayload.iComputerID = os.getComputerID()
	tPayload.bBusy = false
	tPayload.sType = "StorageCellRunner"
	tPayload.iStorageCellBus = tConfig.StorageCellBus

	local tResult = BasketNet2.send(iJobQueuManger, tPayload)
	if tResult ~= nil then
		return true
	end
	
	return false
	
end
