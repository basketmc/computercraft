-- apis/StorageCell

os.loadAPI("apis/BasketNet2")
os.loadAPI("apis/ChestManagement")
os.loadAPI("apis/Database")

local inventory = nil

local iDepth = 0
local iRotation = 0

local tConfig
	
local iMasterControlID
local iJobQueuManger

function init()
	if not rednet.isOpen("left") then
		rednet.open("left")
	end

	while true do
		if turtle.getItemCount(16) > 0 then
			break
		end
		term.clear()
		print("Waiting for a chest in slot 16...")
		sleep(5)		
	end
	
	tConfig = Database.loadTable("config")
	
	term.clear()
	
	while true do
		iMasterControlID = rednet.lookup("BasketNet", "MasterControl")
		if iMasterControlID ~= nil then
			break		
		end
		term.clear()
		print("Unable to connect to Master Control")
		sleep(5)
	end
	
	while true do
		iJobQueuManger = rednet.lookup("BasketNet", "JobQueue")
		if iJobQueuManger ~= nil then
			break		
		end
		term.clear()
		print("Unable to connect to Job Queue Server")
		sleep(5)
	end
	
	while not registerWithJobManager() then
		term.clear()
		print("Unable to register with Job Queue Server")
		sleep(5)
	end
	
	returnHome()

	if turtle.getFuelLevel() < 1000 then
		print("I'm low on fuel")
		return
	end

	inventory = Database.loadTable("inventory")
	if inventory == nil then
		takeInventory()
	end
end

function returnHome()

	iDepth = 0
	iRotation = 0

	while turtle.up() do end
	for i = 1, 4, 1 do
		local status, info = turtle.inspect()
		if status then
			if info.name == "IronChest:BlockIronChest" and info.metadata == 2 then
				return true
			end
		end
		turtle.turnRight()
	end
	return false
end
 
function processRequest()

	local tResponse = BasketNet2.receive(nil, 60)
	
	if tResponse == nil then
		return false
	end
	
	print("Request from " .. tResponse.iSenderID .. ":")
	print("\tAction: " .. tResponse.action)

	if tResponse.action == "busy" then
		tResponse.reply({answer = "no"})
	elseif tResponse.action == "store" then
		storeBuffer()
	elseif tResponse.action == "retrieve" then
		tResponse.reply({action = "retrieving"})
		print("\t\tRetrieving " .. tResponse.amount .. "x " .. tResponse.item.name)
		retrieveItems(tResponse.owner, tResponse.item, tResponse.amount)
	elseif tResponse.action == "reboot" then
		os.reboot()
	end
end

function takeInventory(tJobRequest)

	print("Taking Inventory...")

	inventory = {}

	Database.saveTable("inventory", nil)
	Database.putOnServer("StorageCell" .. tConfig.StorageCell .. "Inventory", inventory)

	returnHome()
	local i = 1

	-- While there is an empty space below the turtle
	while not turtle.detectDown() do
		turtle.turnRight()
		inventory[i] = ChestManagement.whatIsInChest()
		i = i + 1
		if math.fmod(i, 4) == 0 then
			turtle.down()
		end
	end

	Database.saveTable("inventory", inventory)
	Database.putOnServer("StorageCell" .. tConfig.StorageCell .. "Inventory", inventory)

	returnHome()

	displayInventory()

	return true
end

function requestRefueling()

	local tResult
	
	local iAmount = turtle.getFuelMax() - turtle.getFuelLevel()

	while true do
		tResult = BasketNet2.send(iMasterControlID, {action = "fuelrequest", amount = iAmount})
		if tResult ~= nil then
			break		
		end
		print("Unable to send fuel request")
	end
	
end

function refuel(tJobRequest)

	returnHome()
	
	while turtle.getFuelLevel() < turtle.getFuelLimit() do
	
		for i = 1, 15, 1 do
			turtle.select(i)
			turtle.suck()
			if turtle.getFuelLevel() < turtle.getFuelLimit() then
				turtle.refuel()
			else
				break
			end
		end
	
		for i = 1, 15, 1 do
			turtle.select(i)
			turtle.drop()
		end
		
	end

end

function retrieveItemByChestID(iChestID, iAmount)

	local tTargetItem

	for tItems in pairs(inventory[iChestID]) do
		for tTargetItem in tItems do
			break
		end
	end

	return retrieveItems(tTargetItem, iAmount)

end

function retrieveItems(tTargetItem, iAmount)

	if tTargetItem ~= nil then
		for iChestID, tItems in pairs(inventory) do
			for k, tItemTest in pairs(tItems) do
				if ChestManagement.itemsAreTheSame(tTargetItem, tItemTest) then
					goToChest(iChestID)
					for i = 1, 15, 1 do
						turtle.select(i)
						if turtle.suck() then
							iAmount = iAmount - turtle.getItemCount(i)
							
							if iAmount < 0 then
								turtle.dropUp(0 - iAmount)
								iAmount = 0
							end

							inventory[iChestID][k].count = inventory[iChestID][k].count - turtle.getItemCount(i)
							if inventory[iChestID][k].count <= 0 then
								table.remove(inventory[iChestID], k)
							end

							if iAmount == 0 then
								break
							end
						end
					end

					Database.saveTable("inventory", inventory)
					Database.putOnServer("StorageCell" .. tConfig.StorageCell .. "Inventory", inventory)
					
					if iAmount == 0 or turtle.getItemCount(15) > 0 then
						returnHome()
						for i = 1, 15, 1 do
							if turtle.getItemCount(i) > 0 then
								turtle.select(i)
								if not turtle.drop() then
									-- Buffer is full!
								end
							end
						end
					end
				end
				break
			end

			if iAmount == 0 then
				return true
			end
		end
	end
end

function storeBuffer(tJobRequest)

	if turtle.getFuelLevel() < 1000 then
		print("I'm low on fuel")
		returnHome()
		return false
	end

	print("Storing Buffer...")

	while true do

		returnHome()

		for i = 1, 15, 1 do
			turtle.select(i)
			if not turtle.suck() then
				i = i - 1
				if i >= 1 and turtle.getItemDetail(i) ~= nil then
					print("\tGrabbed " .. i .. " stack(s) of " .. turtle.getItemDetail(i).name)
				end
				break
			end

			if i > 1 and not ChestManagement.itemsAreTheSame(turtle.getItemDetail(1), turtle.getItemDetail(i)) then
				turtle.drop()
				i = i - 1
				if i >= 1 and turtle.getItemDetail(i) ~= nil then
					print("\tGrabbed " .. i .. " stack(s) of " .. turtle.getItemDetail(i).name)
				end
				break
			end
		end

		-- If there is nothing in the buffer
		if turtle.getItemCount(1) == 0 then
			break
		end

		local chest = nil

		local bFinished = false

		for iPass = 1, 2, 1 do

			for chest, items in pairs(inventory) do	

				if bFinished then
					break
				end

				local bPutInThisChest = false
				local bChestIsFull = false
	
				local iFirstSlotWithItems = 1
				for iFirstSlotWithItems = 1, 15, 1 do
					if turtle.getItemCount(i) > 0 then
						break
					end
				end

				-- The first pass look for chests w/ that item already
				if iPass == 1 then
					for k, tItem1 in pairs(items) do					
						if ChestManagement.itemsAreTheSame(tItem1, turtle.getItemDetail(iFirstSlotWithItems)) then
							bPutInThisChest = true
							break
						end
					end
				else
					if #items == 0 then
						bPutInThisChest = true
					end
				end

				if bPutInThisChest then

					print("\tHeading to chest " .. chest)

					-- Move the turtle up or down to the destination chest
					while iDepth ~= math.floor(chest / 4) do
						if iDepth > math.floor(chest / 4) then
							turtle.up()
							iDepth = iDepth - 1
						else
							turtle.down()
							iDepth = iDepth + 1
						end
					end

					-- Rotate the turtle to the destination chest
					while iRotation ~= math.fmod(chest, 4) do
						--if iRotation - math.fmod(chest, 4) == 3 then
						--	turtle.turnLeft()
						--	iRotation = iRotation - 1
						--else
							turtle.turnRight()
							iRotation = iRotation + 1
						--end
						iRotation = math.fmod(iRotation, 4)
					end

					bFinished = true

					for i = 1, 15, 1 do
						if turtle.getItemCount(i) > 0 then
							local info = turtle.getItemDetail(i)
							if info ~= nil then
								turtle.select(i)
								if not turtle.drop() then
									bFinished = false
									break
								end

								local iAmountRemaining = turtle.getItemCount(i)

								local added = false

								for k, tItem in pairs(inventory[chest]) do
									if ChestManagement.itemsAreTheSame(tItem, info) then
										inventory[chest][k].count = inventory[chest][k].count + info.count - iAmountRemaining
										Database.saveTable("inventory", inventory)
										Database.putOnServer("StorageCell" .. tConfig.StorageCell .. "Inventory", inventory)
										added  = true
										break
									end
								end

								if not added then
									table.insert(inventory[chest], info)
									Database.saveTable("inventory", inventory)
									Database.putOnServer("StorageCell" .. tConfig.StorageCell .. "Inventory", inventory)
								end
							end
						end
					end
				end

			end

		end

		if not bFinished then
			print("Unable to find chest to put these items in")
			return false
		end

	end

end

function goToChest(iChestID)

	-- Move the turtle up or down to the destination chest
	while iDepth ~= math.floor(iChestID / 4) do
		if iDepth > math.floor(iChestID / 4) then
			turtle.up()
			iDepth = iDepth - 1
		else
			turtle.down()
			iDepth = iDepth + 1
		end
	end

	-- Rotate the turtle to the destination chest
	while iRotation ~= math.fmod(iChestID, 4) do
		--if iRotation - math.fmod(iChestID, 4) == 3 then
		--	turtle.turnLeft()
		--	iRotation = iRotation - 1
		--else
		turtle.turnRight()
		iRotation = iRotation + 1
		--end
		iRotation = math.fmod(iRotation, 4)
	end

end

function registerWithJobManager()

	local tPayload = {}
	
	tPayload.iComputerID = os.getComputerID()
	tPayload.bBusy = false
	tPayload.sType = "StorageCell"
	tPayload.iStorageCell = tConfig.StorageCell
	tPayload.iStorageCellBus = math.floor(tConfig.StorageCell / 4)

	local tResult = BasketNet2.send(iJobQueuManger, tPayload)
	if tResult ~= nil then
		return true
	end
	
	return false
	
end

function displayInventory()
	for i = 1, #inventory do
		print(" ");
		print("Chest ", i, ":");
		for k, v in pairs(inventory[i]) do
			print(v, "x ", k)
		end
	end
end
