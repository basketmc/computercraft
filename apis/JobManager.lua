-- apis/JobQueue

os.loadAPI("apis/BasketNet2")

-- table, indexed by priority, of tables of jobs
local tJobQueue = {}

-- index individual job keys to what main job key it's inside
local tJobsIndexes = {}

-- Table of workers
local tWorkers = {}

--
-- Structure of a job
--
--	{
--		sJobKey: abcde   
--		iJobRequester: 55
--		iJobRecipient: 56
--		tTaskCommands: {
--			"Get BasketMC 64"
--			{example table object}
--		sTaskDescription: "Get 64x Cobble" 
--		tSubJobs: {} -- subarray of jobs
--	}
--

function init()
	if not rednet.isOpen("left") then
		rednet.open("left")
	end

	BasketNet2.registerAs("JobQueue")
end

function processRequest()

	local tResponse = BasketNet2.receive(3)
	
	if tResponse == nil then
		return false
	end
	
	print("Request from " .. tResponse.iSenderID .. ":")
	print("\tAction: " .. tResponse.action)
	
	if tResponse.action == "create" then
		createJob(tResponse)
	
	elseif tResponse.action == "finish" and tResponse.jobid ~= nil then
		print("\tJobID: " .. tResponse.jobid)
		finishJob(tResponse)
	
	elseif tResponse.action == "list" then
		tResponse.reply({list = getJobList()})
		
	elseif tResponse.action == "register" then
		tWorkers[tResponse.iSenderID] = tResponse.tPayload;
		
	elseif sMessage == "Reboot" then
		os.reboot()
		
	end
end

function fetchNewJobs()

	-- contact master control for new jobs
	
end

function processJobQueue(iPriorityLevel)

	if tJobQueue[iPriorityLevel] == nil then
		return true
	end

	for sJobKey, tParentJob in pairs(tJobQueue[iPriorityLevel]) do
		if not tParentJob.bStarted then
			if tParentJob.startJob() then
				tParentJob.bStarted = true
			end
		end	
	end	

end

-- Delete a main job and related index pointers
function deleteJob(sTargetJobKey)

	for iPriorityLevel, tJobs in pairs(tJobQueue) do
		tJobQueue[iPriorityLevel].remove(sTargetJobKey)
	end
	
	for sChildJobKey, sParentJobKey in pairs(tJobsIndexes) do
		if sParentJobKey == sTargetJobKey then
			tJobsIndexes.remove(sChildJobKey)
		end
	end

end

function finishJob(tResponse)

end

function isTargetReady(iTarget)

	local tResult = BasketNet2.send(iTarget, {action = "busy"}, nil, 1, 1)
	
	if tResult == nil then
		return false
	end
	
	if tResult.answer == "no" then
		return true
	end
	
	return false

end

function createJobKey(l)
	if l < 1 then
		l = 8
	end
	local sKey = ""
	while true do
		for i = 1, l do
			sKey = sKey .. string.char(math.random(string.byte("a"), string.byte("z")))
		end
		
		-- If this key isn't already taken
		if tJobsIndexes[sKey] == nil then
			break
		end
	end
    return sKey
end