-- apis/Turtle2

os.loadAPI("apis/Coordinates")
os.loadAPI("apis/Database")

local tCurrentLocation = nil

function init()
	while true do
		tCurrentLocation = Coordinates.getCurrentLocation()
		if tCurrentLocation == nil then
			print("Unable to find GPS signal, trying again in 5 seconds...")
			sleep(5)
		else
			break
		end
	end
end

function getCurrentLocation()
	return tCurrentLocation
end

function turnRight()
	if tCurrentLocation ~= nil and tCurrentLocation.r ~= nil then
		tCurrentLocation.r = math.fmod(tCurrentLocation.r + 1, 4)
	end
	turtle.turnRight()	
end

function turnLeft()
	if tCurrentLocation ~= nil and tCurrentLocation.r ~= nil then
		tCurrentLocation.r = math.fmod(tCurrentLocation.r - 1, 4)
	end
	turtle.turnLeft()
end

function up()
	if turtle.up() then
		tCurrentLocation.y = tCurrentLocation.y + 1
		return true
	end
	return false
end

function down()
	if turtle.down() then
		tCurrentLocation.y = tCurrentLocation.y - 1
		return true
	end
	return false
end

function forward()
	if turtle.forward() then
		if tCurrentLocation.r == 0 then
			tCurrentLocation.z = tCurrentLocation.z - 1
		elseif tCurrentLocation.r == 1 then
			tCurrentLocation.x = tCurrentLocation.x + 1
		elseif tCurrentLocation.r == 2 then
			tCurrentLocation.z = tCurrentLocation.z + 1
		elseif tCurrentLocation.r == 3 then
			tCurrentLocation.x = tCurrentLocation.x - 1
		end
			
		return true
	end
	return false
end

function getCoords(key)
	if key == nil then
		key = "home"
	end
	local tConfig = Database.loadTable("config")
	if tConfig == nil
		or tConfig[key] == nil  
		or tConfig[key] == nil 
		or type(tConfig[key]) ~= "table"
		or tConfig[key].x == nil
		or tConfig[key].y == nil
		or tConfig[key].z == nil
		or tConfig[key].r == nil then
		return nil
	else
		return Coordinates.createCoordinates(tConfig[key])
	end
end

function setCoords(key, coords2)
	if key == nil then
		key = "home"
	end
	
	local coords = {}
	
	if coords2 == nil then
		coords.x, coords.y, coords.z = gps.locate()
		
		if tCurrentLocation == nil or tCurrentLocation.r == nil then
			determineFacingDirection()
		end
		
		coords.r = tCurrentLocation.r
	else
		coords.x = coords2.x
		coords.y = coords2.y
		coords.z = coords2.z
		coords.r = coords2.r
	end
	
	local tConfig = Database.loadTable("config")
	if tConfig == nil then
		tConfig = {}
	end
	tConfig[key] = coords;	
	Database.saveTable("config", tConfig)
	
	return coords
end

function isFacingTurtle()
	local bStatus, tTarget = turtle.inspect()
	if bStatus and tTarget ~= nil and tTarget.name == "ComputerCraft:CC-TurtleExpanded" then
		return true
	else
		return false
	end
end

function isFacingEnderChest()
	local bStatus, tTarget = turtle.inspect()
	if bStatus and tTarget ~= nil and tTarget.name == "ComputerCraft:CC-TurtleExpanded" then
		return true
	else
		return false
	end
end

function determineFacingDirection()
	local tBaseCoords = Coordinates.getCurrentLocation()
	local tComparisonCoords
	local iDiff
	if not turtle.detect() then
		turtle.forward()
		tComparisonCoords = Coordinates.getCurrentLocation()
		iDiff = 0
	else
		turtle.turnRight()
		if not turtle.detect() then
			turtle.forward()
			tComparisonCoords = Coordinates.getCurrentLocation()
			iDiff = -1
		else
			turtle.turnRight()
			if not turtle.detect() then
				turtle.forward()
				tComparisonCoords = Coordinates.getCurrentLocation()
				iDiff = -2
			else
				turtle.turnRight()
				if not turtle.detect() then
					turtle.forward()
					tComparisonCoords = Coordinates.getCurrentLocation()
					iDiff = -3
				else
					print("Unable to find out what direction I'm facing... :( ")
					return 0
				end
			end
		end
	end
	
	if tComparisonCoords == nil then
		print("Unable to find out rotation, assuming north")
		tCurrentLocation.r = 0
		return
	end
	
	if tBaseCoords.x > tComparisonCoords.x then
		tComparisonCoords.r = 3
	elseif tBaseCoords.x < tComparisonCoords.x then
		tComparisonCoords.r = 1
	elseif tBaseCoords.z > tComparisonCoords.z then
		tComparisonCoords.r = 0
	elseif tBaseCoords.z < tComparisonCoords.z then
		tComparisonCoords.r = 2
	end
	
	tCurrentLocation = tComparisonCoords
	
	-- Assign the original rotation to the base coords
	tBaseCoords.r = math.fmod(tComparisonCoords.r + iDiff + 4, 4)
	
	goToAndRotate(tBaseCoords)

end

function goTo(coords, bAggressiveGoTo)

	print("goTo " .. coords.print())
	print(bAggressiveGoTo)
	sleep(1)
	
	if tCurrentLocation.r == nil and (tCurrentLocation.x ~= coords.x or tCurrentLocation.z ~= coords.z or tCurrentLocation.r ~= coords.r) then
		determineFacingDirection()
	end
	
	while tCurrentLocation.x ~= coords.x or tCurrentLocation.y ~= coords.y or tCurrentLocation.z ~= coords.z do
		
		-- Go up/down
		while tCurrentLocation.y ~= coords.y do
			if tCurrentLocation.y > coords.y then
				if bAggressiveGoTo and turtle.detectDown() then
					turtle.digDown()
				end
				if not down() then
					if bAggressiveGoTo then
						turtle.attackDown()
					else
						break
					end
				end
			else
				if bAggressiveGoTo and turtle.detectUp() then
					turtle.digUp()
				end
				if not up() then
					if bAggressiveGoTo then
						turtle.attackUp()
					else
						break
					end
				end
			end
		end
		
		-- go along the x axis, east/west
		if tCurrentLocation.x ~= coords.x then	
			if tCurrentLocation.x > coords.x then
				rotateTo(3) -- west
			else
				rotateTo(1) -- east
			end
		
			while tCurrentLocation.x ~= coords.x do
				if bAggressiveGoTo and turtle.detect() then
					turtle.dig()
				end
				if not forward() then
					if bAggressiveGoTo then
						turtle.attack()
					else
						break
					end
				end
			end
		end
		
		-- go along the z axis, north/south
		if tCurrentLocation.z ~= coords.z then
			if tCurrentLocation.z > coords.z then
				rotateTo(0) -- north
			else
				rotateTo(2) -- south
			end	
		
			while tCurrentLocation.z ~= coords.z do
				if bAggressiveGoTo and turtle.detect() then
					turtle.dig()
				end
				if not forward() then
					if bAggressiveGoTo then
						turtle.attack()
					else
						break
					end
				end
			end
		end
		
	end

end

function goToAndRotate(coords, bAggressiveGoTo)
	goTo(coords, bAggressiveGoTo)
	rotateTo(coords.r)
end

function rotateTo(iTargetRotation)

	local iDiff = math.fmod(iTargetRotation - tCurrentLocation.r + 4, 4)
	
	if iDiff == 1 then
		turnRight()
	elseif iDiff == 2 then
		turnRight()
		turnRight()
	elseif iDiff == 3 then
		turnLeft()
	end

end
