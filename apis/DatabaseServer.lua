-- apis/DatabaseServer

os.loadAPI("apis/BasketNet2")
os.loadAPI("apis/ChestManagement")
os.loadAPI("apis/Database")

local tDatabase = nil

function init()
	if not rednet.isOpen("left") then
		rednet.open("left")
	end
	
	tDatabase = Database.loadTable("database")
	if tDatabase == nil then
		tDatabase = {}
		Database.saveTable("database", tDatabase)
	end
	
	rednet.host("BasketNet", "DatabaseServer")
end
 
function processRequest()

	local tResponse = BasketNet2.receive(nil, 60)
	if tResponse ~= nil then
	
		print("Request from " .. tResponse.iSenderID .. ":")
		print("\tAction: " .. tResponse.action)
	
		if tResponse.action == "reboot" then
			os.reboot()
		elseif tResponse.action == "get" then
			print("\tGet " .. tResponse.name)
			tResponse.reply(tDatabase[tResponse.name])
		elseif tResponse.action == "put" then
			print("\tPut " .. tResponse.name)
			tDatabase[tResponse.name] = tResponse.value
		end
	end
	
end
