-- apis/Terminal

os.loadAPI("apis/BasketNet2")
os.loadAPI("apis/Database")
os.loadAPI("apis/ChestManagement")

function init()
	if not rednet.isOpen("left") then
		rednet.open("left")
	end
	
	tConfig = Database.loadTable("config")
end

-- Get the table object describing the output chest
function getOutputChest()

	if tConfig.outputChest ~= nil then
		return tConfig.outputChest
	end

	term.clear()

	print("Output Chest Not Configured Menu...")
	
	tConfig.outputChest = ChestManagement.createEnderChestConfig()
	
	Database.saveTable("config", tConfig)
	
	return tConfig.outputChest
	
end


-- Get the table object describing the input chest
function getInputChest()

	if tConfig.inputChest ~= nil then
		return tConfig.inputChest
	end

	term.clear()

	print("Output Chest Not Configured Menu...")
	
	tConfig.inputChest = ChestManagement.createEnderChestConfig()
	
	Database.saveTable("config", tConfig)
	
	return tConfig.inputChest
	
end
