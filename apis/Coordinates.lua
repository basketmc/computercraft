-- apis/Coordinates

function getCurrentLocation()
	local x, y, z = gps.locate()
	if x == nil then
		return nil
	else
		return createCoordinates(x, y, z)
	end
end

function createCoordinates(x, y, z, r)

	if type(x) == "table" then
		r = x.r	
		z = x.z
		y = x.y
		x = x.x	
	end

	local coords = {}
	coords.x = x;
	coords.y = y;
	coords.z = z;
	coords.r = r;
	
	coords.isEqual = function(coords2)
		if coords.x == coords2.x and coords.y == coords2.y and coords.z == coords2.z then
			return true
		else
			return false
		end
	end
	
	coords.print = function()
		if coords.r == nil then
			return coords.x .. ", " .. coords.y .. ", " .. coords.z .. ", nil"	
		else
			return coords.x .. ", " .. coords.y .. ", " .. coords.z .. ", " .. coords.r
		end
	end
	
	coords.betweenPoints = function(point1, point2)
		if point1.y == point2.y and point1.y == point2.y and math.min(point1.x, point2.x) < coords.x and math.max(point1.x, point2.x) > coords.x then
			return true
		end
		if point1.x == point2.x and point1.z == point2.z and math.min(point1.y, point2.y) < coords.y and math.max(point1.y, point2.y) > coords.y then
			return true
		end
		if point1.x == point2.x and point1.y == point2.y and math.min(point1.z, point2.z) < coords.z and math.max(point1.z, point2.z) > coords.z then
			return true
		end
		return false
	end
	
	coords.clone = function()
		return createCoordinates(coords.x, coords.y, coords.z, coords.r)	
	end
	
	return coords

end