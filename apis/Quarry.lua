-- apis/StorageCell

os.loadAPI("apis/BasketNet2")
os.loadAPI("apis/Database")
os.loadAPI("apis/Turtle2")
os.loadAPI("apis/Coordinates")

local iChest = 15
local iTorches = 16

local tConfig

function init()

	if not rednet.isOpen("left") then
		rednet.open("left")
	end
	
	tConfig = Database.loadTable("config")
	
	term.clear()
	
	Turtle2.init()
	Turtle2.determineFacingDirection()
	
end

function run()
	
	if Turtle2.getCoords("home") == nil then
		if not mainMenu() then
			return
		end
		print("Beginning Quarry...")
	else	
		print("Resuming Previous Quarry...")	
		sleep(1)
	end
	
	doQuarry()

	-- When complete
	returnHome()
		
	local tConfig = Database.loadTable("config")
	if tConfig == nil then
		tConfig = {}
	end
	tConfig.home = nil
	tConfig.progress = nil
	tConfig.destination = nil		
	Database.saveTable("config", tConfig)
	
end

function mainMenu()

	while true do
	
		print("Menu:")

		print(" 1. Start a new Quarry")

		print(" 0. Exit")

		choice = read()

		if choice == "0" then
			return false
			
		elseif choice == "1" then
		
			print("How far forward?")
			
			local iForward = read()
		
			print("How far Right?")
			
			local iRight = read()
		
			print("How far Down?")
			
			local iDepth = read()
			
			local tDestination = Turtle2.getCurrentLocation().clone()
		
			if Turtle2.getCurrentLocation().r == 0 then			
				tDestination.x = tDestination.x + iRight			
				tDestination.z = tDestination.z - iForward
				tDestination.y = tDestination.y - iDepth			
			elseif Turtle2.getCurrentLocation().r == 1 then		
				tDestination.x = tDestination.x + iForward			
				tDestination.z = tDestination.z + iRight
				tDestination.y = tDestination.y - iDepth			
			elseif Turtle2.getCurrentLocation().r == 2 then		
				tDestination.x = tDestination.x - iRight		
				tDestination.z = tDestination.z + iForward
				tDestination.y = tDestination.y - iDepth			
			elseif Turtle2.getCurrentLocation().r == 3 then	
				tDestination.x = tDestination.x - iForward	
				tDestination.z = tDestination.z - iRight	
				tDestination.y = tDestination.y - iDepth			
			end
			
			-- Can't dig below bedrock
			if tDestination.y < 1 then
				tDestination.y = 1
			end
			
			Turtle2.setCoords("home")
			
			Turtle2.setCoords("destination", tDestination)
			
			return true
					
		end
		
	end	

end

function returnHome()
	Turtle2.goToAndRotate(Turtle2.getCoords("home"), true)
end
 
function doQuarry()

	local tProgress = Turtle2.getCoords("progress")
	if tProgress == nil then
		tProgress = Turtle2.getCurrentLocation().clone()
	end
	
	local tHome = Turtle2.getCoords("home")
	local tDestination = Turtle2.getCoords("destination")
	
	Turtle2.goTo(tProgress, true)

	while true do	

		if turtle.getFuelLevel() < 1000 then
			print("I'm low on fuel")
			return
		end
		
		tProgress = Turtle2.getCurrentLocation().clone()		
		Turtle2.getCoords("progress", tProgress)
		
		local target = tProgress.clone()
				
		if tProgress.x == tHome.x then
			target.x = tDestination.x
		elseif tProgress.x == tDestination.x then
			target.x = tHome.x
		end
		
		Turtle2.goTo(target, true)
		
		if (math.fmod(tHome.y - target.y, 2) == 0 and target.z == tDestination.z) or
			(math.fmod(tHome.y - target.y, 2) == 1 and target.z == tHome.z) then
			
			if target.y == tDestination.y then
				return
			end			
			target.z = target.z - 1
			Turtle2.goTo(target, true)			
		else 				
			if math.fmod(tHome.y - target.y, 2) == 0 then
				if tHome.z > tDestination.z then
					target.z = target.z - 1
				else
					target.z = target.z + 1
				end
			else
				if tHome.z > tDestination.z then
					target.z = target.z + 1
				else
					target.z = target.z - 1
				end
			end
		end
		
		Turtle2.goTo(target, true)
		
	end

end

function emptyInventory()


end

 
