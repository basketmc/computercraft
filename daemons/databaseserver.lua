os.loadAPI("apis/DatabaseServer")

term.clear()
print("Ready for requests...")

DatabaseServer.init()

while true do
	DatabaseServer.processRequest()
end
