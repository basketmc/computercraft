os.loadAPI("apis/Lumberjack")
os.loadAPI("apis/ChestManagement")

term.clear()
print("I'm a lumberjack, and that's ok!")

while true do
	if turtle.getItemCount(1) > 0 then
		break
	end
	print("Waiting for saplings in my first inventory slot...")
	sleep(5)
end

while turtle.down() do end

while true do

	if turtle.getFuelLevel() < 500 then
		print("I'm low on fuel")
		return
	end

	turtle.select(1)
	
	-- Fill up the inventory
	for i = 1, 16, 1 do
		turtle.suckDown()
	end
	
	-- Put everything back that's not in the first slot
	for i = 2, 16, 1 do
		turtle.select(i)
		turtle.dropDown()
	end

	while not turtle.up() do
		turtle.digUp()
		turtle.attackUp()
	end

	while not turtle.up() do
		turtle.digUp()
		turtle.attackUp()
	end
	
	if turtle.detect() then
		turtle.down()
		while turtle.detect() do
			turtle.dig()
			turtle.digUp()
			turtle.up()
		end
		
		while turtle.down() do end
		
		turtle.up()
		
		-- Place new sapling
		turtle.select(1)
		turtle.place()
		
		turtle.suckDown()
	end
	
	while turtle.down() do end
	
	sleep(120)

end