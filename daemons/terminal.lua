os.loadAPI("apis/Terminal")
os.loadAPI("apis/BasketNet")
os.loadAPI("apis/Database")
os.loadAPI("apis/Job")

term.clear()
 
Terminal.init()

local choice = "mainmenu"

local sLoggedInUser = "BasketMC"

local tCurrentJob = nil
 
while true do

	if choice == "waitingforjob" then
	
		term.clear()
		
		print("Waiting for current job to complete...")
		
		local tResponse = tCurrentJob.receive(nil, 30)
		
		if tResponse ~= nil then
			if tResponse.tPayload.action == "JobComplete" then
			print("Job has complete!")
			choice = "mainmenu"
			tCurrentJob = nil
			end
		end

	elseif choice == "mainmenu" then

		term.clear()

		print("Main Menu:")

		print(" 1. Deposit Items")

		print(" 2. Request Items")

		print(" 3. Reboot the system")

		print(" 0. Exit")

		choice = read()

		if choice == "0" then
			return
		elseif choice == "1" then
			choice = "deposit"

		elseif choice == "2" then
			choice = "request"

		elseif choice == "3" then
			choice = "reboot"
		end
		
	elseif choice == "deposit" then
	
		local tRequest = {}
		tRequest.action = "deposit"
		tRequest.source = Terminal.getOutputChest()

		local iAttempts = 0
		while iAttempts < 5 do
			local tResult = BasketNet2.send(BasketNet2.getComputerID("MasterControl"), tRequest);
			if tResult ~= nil then
			
			
				tCurrentJob = tResult
				choice = "waitingforjob"
				break
			end
		end

	elseif choice == "request" then

		term.clear()
		print("Loading...")

		local tInventory = {}

		local iAttempts = 0
		while iAttempts < 5 do
			BasketNet.sendToMasterControl("GetInventory " .. sLoggedInUser, "BasketNet")
			local iSenderID, sMessage, sProtocol = BasketNet.receiveFromMasterControl(10, "BasketNet")
			if iSenderID ~= nil then
				tInventory = sMessage					
				iAttempts = 0
				break
			end
			iAttempts = iAttempts + 1
		end
			
		if iAttempts >= 5 then
			print("Unable to contact Master Control")
			choice = "mainmenu"
			sleep(3)
		else

			local iOffset = 0
			local iHowManyPerPage = 15
			local sFilter = ""

			while true do 

				term.clear()

				local iCounter = 0
				local iListCounter = 1
				local iLimit = iOffset + iHowManyPerPage

				local tItemShortList = {}

				local tFilteredResults = {}

				for k, tItem in ipairs(tInventory) do
					table.insert(tFilteredResults, tItem)
				end

				for k, tItem in ipairs(tFilteredResults) do
					if sFilter == "" or string.match(tItem.name, sFilter) then						
						if iCounter < iLimit and iCounter >= iOffset then
							tItemShortList[iListCounter] = tItem
							print(iListCounter .. ".\t" ..tItem.count .. "x\t" .. tItem.name)
							iListCounter = iListCounter + 1
						end
						iCounter = iCounter + 1
						if iCounter > iLimit then
							break
						end
					end
				end

				print("Usage:")
				print("'search [keyword]'; 'p [page number]'; '[number in front of item]'; exit")

				choice = read()

				local tParsedMessage = {}
				
				for sPart in string.gmatch(choice, "%S+") do
					table.insert(tParsedMessage, sPart)
				end

				if tParsedMessage[1] == "p" then
					iOffset = (tParsedMessage[2] - 1) * iHowManyPerPage
				elseif tParsedMessage[1] == "search" then
					if tParsedMessage[2] == nil then
						sFilter = ""
					else
						sFilter = tParsedMessage[2]
					end
				elseif tParsedMessage[1] == "exit" then
					choice = "mainmenu"
					break
				else
					choice = choice + 0
					if choice > 0 and choice <= iHowManyPerPage then
						if not BasketNet.startConversationWithMasterControl() then
							print("Unable to connect to master control")
							BasketNet.endConversation()
							sleep(2)
							break
						end

						BasketNet.sendToConversation("Retrieve " .. sLoggedInUser .. " 64")
						local iSenderID, sMessage, sProtocol = BasketNet.receiveFromConversation(3)
						if sMessage ~= "Ready" then
							print("Master Control is not ready")
							BasketNet.endConversation()
							sleep(2)
							break
						end

						BasketNet.sendToConversation(tItemShortList[choice])

						if iSenderID ~= nil and sMessage == "Retrieving" then
							print("Unable to retrieve items right now")
							BasketNet.endConversation()
							sleep(4)
							break
						end

						print("Retrieving...")
						BasketNet.endConversation()
						sleep(4)
					end
				end

			end
		end

	elseif choice == "reboot" then

		BasketNet.sendToMasterControl("Reboot", "BasketNet")

		print("Rebooting...")
		sleep(5)

		choice = "mainmenu"

		-- send reboot command

	end
 
end
