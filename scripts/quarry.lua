local iRight = 0
local iForward = 0
local iDepth = 0

local bFacingForward = true

local iTorches = 16
  
write("How far Forward should I go?")

local iForwardTarget = read("*")
iForwardTarget = iForwardTarget + 0;

write("How far Right should I go?")

local iRightTarget = read("*")
iRightTarget = iRightTarget + 0;
 
write("How far Down should I go?")

local iDepthTarget = read("*")
iDepthTarget = iDepthTarget + 0;

function returnToProcessPoint(iForward, iRight, iDepth, bFacingForward)

	print("Returning to Position")

	if iForward > 0 then
		for i = 1, iForward, 1 do
			while not turtle.forward() do
				turtle.attack()
			end
		end
	end

	if iRight > 0 then
    	turtle.turnRight()
		for i = 1, iRight, 1 do
			while turtle.detect() do
				turtle.dig()
			end
			while not turtle.forward() do 
				turtle.attack()
			end
		end
		turtle.turnLeft()
	end

	if iDepth > 0 then
    	for i = 2, iDepth, 1 do
			turtle.digDown()
			while not turtle.down() do
				turtle.attackDown()
			end
		end
	end
	
end

function returnHome(iForward, iRight, iDepth, bFacingForward)

	print("Returning Home")

	if bFacingForward then
		turtle.turnRight()
		turtle.turnRight()
	end

	if iDepth > 1 then
    	for i = 2, iDepth, 1 do
			while not turtle.up() do
				turtle.attackUp()
			end
		end
	end

	if iRight > 0 then
    	turtle.turnRight()
		for i = 1, iRight, 1 do
			while not turtle.forward() do
				turtle.attack()
			end
		end
		turtle.turnLeft()
	end

  	for i = 1, iForward, 1 do
  		while turtle.detect() do
			turtle.dig()
		end
		while not turtle.forward() do
			turtle.attack()
		end
	end

end

while true do

	-- Return to where you left off
	returnToProcessPoint(iForward, iRight, iDepth, bFacingForward)
	
	while true do
	
		turtle.select(1)
			
		if iDepth > 0 then
			turtle.digDown()
		end
	
		-- Do the Digging
		for i = 2, iForwardTarget, 1 do
				
			if iDepth > 0 then
			
				-- Grab a torch in front
				while turtle.detect() do
					turtle.select(iTorches)
					turtle.dig()
				end
				
				while not turtle.forward() do
					turtle.attack()
				end
				
				if bFacingForward then
					iForward = iForward + 1
				else
					iForward = iForward - 1
				end
				
				turtle.digDown()
	
				-- Place torch
				if iDepth > 0 and math.fmod(iForward, 4) == 1 and math.fmod(iRight, 4) == 1 then
					turtle.select(iTorches)
					turtle.placeDown()
				end
			else 
				while turtle.detect() do
					turtle.dig()
				end
			
				while not turtle.forward() do
					turtle.attack()
				end
				if bFacingForward then
					iForward = iForward + 1
				else
					iForward = iForward - 1
				end
				
			end	
			
		end
	
		if turtle.getItemCount(15) == 0 then
		
			iRight = iRight + 1

			if iRight == iRightTarget then
				print("You're at the most right column")
				iRight = iRight - 1
		    	break
			end
		
			if bFacingForward then
				turtle.turnRight()
				if iDepth == 0 then
					while turtle.detect() do
						turtle.dig()
					end
				end
				
				turtle.forward()
				turtle.turnRight()
				
			else
				turtle.turnLeft()
				if iDepth == 0 then
					while turtle.detect() do
						turtle.dig()
					end
				end
				
				turtle.forward()
				turtle.turnLeft()
				
			
			end

			bFacingForward = not bFacingForward;
		
		else
			
			print("Your inventory is full")
			break
		
		end
		
	end

	-- Return back	
	returnHome(iForward, iRight, iDepth, bFacingForward)

	-- Empty Inventory
	for sel = 1, 15 do
    	turtle.select(sel)
    	turtle.drop()
	end

	-- Reset Direction
	turtle.turnRight()
	turtle.turnRight()
	
	iForward = 0

	iRight = iRight + 1
	
	bFacingForward = true

	if iRight == iRightTarget then
    	iDepth = iDepth + 1
    	iRight = 0
	end

	if iDepth == iDepthTarget then
		return
	end

end