
function generateURL(filename, branch)
	return "https://bitbucket.org/basketmc/computercraft/raw/" .. branch .. "/" .. filename .. ".lua?" .. os.day() .. "-" .. os.time()
end

function download(filename, branch)
	print("Downloading " .. filename)
	local data = http.get(generateURL(filename, branch))
	if data then
		print(filename .. " downloaded")
		local file = fs.open(filename, "w")
		file.write(data.readAll())
		file.close()
		return true
	end
end

local branch = "master"

shell.run("mkdir", "daemons")
shell.run("mkdir", "apis")

shell.run("rm", "scripts/quarry")
download("scripts/quarry", branch)

shell.run("rm", "scripts/refuelstation")
download("scripts/refuelstation", branch)

shell.run("rm", "scripts/storagecellutils")
download("scripts/storagecellutils", branch)

shell.run("rm", "scripts/config")
download("scripts/config", branch)

shell.run("rm", "scripts/test")
download("scripts/test", branch)

shell.run("rm", "scripts/sethome")
download("scripts/sethome", branch)

shell.run("rm", "daemons/storagecell")
shell.run("rm", "daemons/mastercontrol")
shell.run("rm", "daemons/terminal")
shell.run("rm", "daemons/databaseserver")
shell.run("rm", "daemons/lumberjack")
shell.run("rm", "daemons/busrunner")
shell.run("rm", "daemons/jobmanager")
shell.run("rm", "daemons/quarry")

shell.run("rm", "apis/BasketNet")
shell.run("rm", "apis/BasketNet2")
shell.run("rm", "apis/StorageCell")
shell.run("rm", "apis/ChestManagement")
shell.run("rm", "apis/MasterControl")
shell.run("rm", "apis/Terminal")
shell.run("rm", "apis/Database")
shell.run("rm", "apis/DatabaseServer")
shell.run("rm", "apis/JobManager")
shell.run("rm", "apis/BusRunner")
shell.run("rm", "apis/Turtle2")
shell.run("rm", "apis/Coordinates")
shell.run("rm", "apis/Quarry")

download("daemons/storagecell", branch)
download("daemons/mastercontrol", branch)
download("daemons/terminal", branch)
download("daemons/databaseserver", branch)
download("daemons/lumberjack", branch)
download("daemons/busrunner", branch)
download("daemons/jobmanager", branch)
download("daemons/quarry", branch)

download("apis/BasketNet", branch)
download("apis/BasketNet2", branch)
download("apis/StorageCell", branch)
download("apis/ChestManagement", branch)
download("apis/MasterControl", branch)
download("apis/Terminal", branch)
download("apis/Database", branch)
download("apis/DatabaseServer", branch)
download("apis/JobManager", branch)
download("apis/BusRunner", branch)
download("apis/Turtle2", branch)
download("apis/Coordinates", branch)
download("apis/Quarry", branch)

if os.getComputerLabel() == "StorageCell1" then
	shell.run("daemons/storagecell")
end

if os.getComputerLabel() == "BusRunner" then
	shell.run("daemons/busrunner")
end

if os.getComputerLabel() == "MasterControl" then
	shell.run("daemons/mastercontrol")
end

if os.getComputerLabel() == "JobManager" then
	shell.run("daemons/jobmanager")
end

if os.getComputerLabel() == "Terminal1" then
	shell.run("daemons/terminal")
end

if os.getComputerLabel() == "DatabaseServer" then
	shell.run("daemons/databaseserver")
end

if os.getComputerLabel() == "Quarry" then
	shell.run("daemons/quarry")
end

if os.getComputerLabel() == "Lumberjack" then
	shell.run("daemons/lumberjack")
end