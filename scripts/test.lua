
local test1 = {}

test1.name = "fred1"

print(test1.name)

local test2 = test1

local test1 = {}

test1.name = "fred3"

print(test2.name)

print(test1.name)

test2.name = "fred2"

print(test2.name)

print(test1.name)