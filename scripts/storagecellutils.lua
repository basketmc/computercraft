os.loadAPI("apis/StorageCell")

term.clear()

local choice = "mainmenu"
 
while true do

	if choice == "mainmenu" then

		term.clear()

		print("Main Menu:")

		print(" 1. Expand Storage Cell")

		print(" 0. Exit")

		choice = read()

		if choice == "0" then
			return
		elseif choice == "1" then
			choice = "expand"
		end

	elseif choice == "expand" then

		term.clear()

		while true do
			if turtle.getItemCount(1) > 0 then
				break
			else
				print("Please place a stack of chests in the first slot")
				sleep(5)
			end
		end
	
		print("Expanding...")
		
		-- Go down all the way
		while turtle.down() do end
		
		-- Grab the bottom lighting
		turtle.select(2)
		turtle.digDown()
		
		turtle.select(1)
		
		while turtle.getItemCount(1) > 3 do
			for i = 1, 4 do
				turtle.dig()
				turtle.place()
				turtle.turnRight()
			end				
			turtle.digDown()
			turtle.down()
		end
		
		-- Place bottom lighting
		turtle.select(2)
		turtle.digDown()
		turtle.placeDown()
		
		StorageCell.returnHome()
		
		for i = 1, 15 do
			turtle.select(i)
			turtle.drop()
		end
		
		choice = "mainmenu"

	end
 
end
