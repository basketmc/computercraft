os.loadAPI("apis/Database")

term.clear()

local choice = "mainmenu"

local tConfig = Database.loadTable("config")

if tConfig == nil then
	tConfig = {}
	Database.saveTable("config", tConfig)
end
 
while true do

	if choice == "mainmenu" then

		term.clear()

		print("Main Menu:")

		print("1. Add Config Value")

		print("2. Remove Config Value")

		print("3. List Config Values")

		print("0. Exit")

		choice = read()

		if choice == "0" then
			return
		elseif choice == "1" then
			choice = "add"
		elseif choice == "2" then
			choice = "remove"
		elseif choice == "3" then
			choice = "list"
		end

	elseif choice == "list" then

		term.clear()
		
		for k, v in pairs(tConfig) do
			print(k .. ": " .. v)
		end
		
		print("Hit Enter to exit")
		
		local sValue = read()
		
		choice = "mainmenu"

	elseif choice == "add" then

		term.clear()
		
		print("What config KEY would you like to use:")
		
		local sKey = read()
		
		print("What config VALUE would you like to use:")
		
		local sValue = read()
		
		tConfig[sKey] = sValue
		
		Database.saveTable("config", tConfig)
		
		choice = "mainmenu"

	elseif choice == "remove" then

		term.clear()
		
		print("What config KEY would you like to DELETE?")
		
		local sKey = read()
		
		table.remove(tConfig, sKey)
		
		Database.saveTable("config", tConfig)
		
		choice = "mainmenu"

	end
 
end
