while turtle.getFuelLevel() < turtle.getFuelLimit() do
	turtle.select(1)
	turtle.suckDown()
	turtle.refuel()	
	turtle.dropUp()
end

print("Fuel Level: " .. turtle.getFuelLevel())
print("Done!")