shell.run("rm", "startup2")
shell.run("rm", "scripts/startup2")

function generateURL(filename, branch)
	return "https://bitbucket.org/basketmc/computercraft/raw/" .. branch .. "/" .. filename .. ".lua?" .. os.day() .. "-" .. os.time()
end

function download(filename, branch)
	print("Downloading " .. filename)
	local data = http.get(generateURL(filename, branch))
	if data then
		print(filename .. " downloaded")
		local file = fs.open(filename, "w")
		file.write(data.readAll())
		file.close()
		return true
	end
end

shell.run("mkdir", "scripts")

download("scripts/startup2", "master")

shell.run("scripts/startup2")
